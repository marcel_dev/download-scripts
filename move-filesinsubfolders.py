#!/usr/bin/python3
# move-filesinsubfolders.py
#
# Move files downloaded in subfolders to folder name.
#
# Marcel Gerber http://it.marcelgerber.ch
#
__version__ = "1.0.0"
__date__    = "Feb. 2018"

import os, argparse, shutil

downloadPath = "~/Downloads/deluge/complete/"
extensions   = 'mkv', 'mp4', 'avi'

parser = argparse.ArgumentParser(description='Move files downloaded in subfolders to folder name.')
parser.add_argument('-d', '--download-path', type=str, default=downloadPath, help='Set download path (default: {})'.format(downloadPath))
parser.add_argument('-f', '--filter', type=str, default='', help='Filter according to pattern.')
args = parser.parse_args()

def log(msg, *params):
    print(msg.format(*params))

downloadPath = os.path.expanduser(args.download_path)
for downloadItem in os.listdir(downloadPath):
    dlItemFullPath = os.path.join(downloadPath, downloadItem)
    if os.path.isdir(dlItemFullPath) and args.filter in dlItemFullPath:
        log("Entering '{}'.", downloadItem)
        for dlFile in os.listdir(dlItemFullPath):
            # log("File: '{}'", dlFile)
            if dlFile.endswith(extensions):
                dlFileBaseName, dlFileExt = os.path.splitext(dlFile)
                dlFileFullPath = os.path.join(dlItemFullPath, dlFile)
                dlNewFileName = dlItemFullPath + dlFileExt
                log("Moving file '{}' to '{}'.", dlFileFullPath, dlNewFileName)
                try:
                    shutil.move(dlFileFullPath, dlNewFileName)
                    log("Completed")
                except Exception as e:
                    log("Error: Could not move file '{}'. {}", dlFileFullPath, e.strerror)
